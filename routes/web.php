<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['prefix'=>'main', 'middleware'=>['auth']], function () {
    Route::get('user/profile', [App\Http\Controllers\UserController::class, 'showProfile'])->name('profile.show');
    Route::put('user/profile', [App\Http\Controllers\UserController::class, 'updateProfile'])->name('profile.update');

    Route::get('service/{id}/print', [App\Http\Controllers\ServiceController::class, 'print'])->name('service.print');
    Route::get('service/pickup', [App\Http\Controllers\ServiceController::class, 'pickupIndex'])->name('service.pickup.index');
    Route::post('service/pickup', [App\Http\Controllers\ServiceController::class, 'pickupStore'])->name('service.pickup.store');

    Route::get('spare_part/order', [App\Http\Controllers\SparePartController::class, 'orderIndex'])->name('spare_part.order.index');
    Route::post('spare_part/order', [App\Http\Controllers\SparePartController::class, 'orderStore'])->name('spare_part.order.store');

    Route::get('spare_part/received', [App\Http\Controllers\SparePartController::class, 'receivedIndex'])->name('spare_part.received.index');
    Route::post('spare_part/received', [App\Http\Controllers\SparePartController::class, 'receivedStore'])->name('spare_part.received.store');

    Route::get('spare_part/fixing', [App\Http\Controllers\SparePartController::class, 'fixingIndex'])->name('spare_part.fixing.index')->middleware('role:technician');
    Route::post('spare_part/fixing', [App\Http\Controllers\SparePartController::class, 'fixingStore'])->name('spare_part.fixing.store')->middleware('role:technician');


    Route::resource('user', App\Http\Controllers\UserController::class)->middleware('role:owner');
    Route::resource('service', App\Http\Controllers\ServiceController::class)->middleware('role:admin');
    Route::resource('customer', App\Http\Controllers\CustomerController::class)->middleware('role:admin');
    Route::resource('symptom', App\Http\Controllers\SymptomController::class)->middleware('role:admin|technician');
    Route::resource('report', App\Http\Controllers\ReportController::class)->middleware('role:admin|owner');
});