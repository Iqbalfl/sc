<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'service_100' => Service::whereStatus(100)->count(),
            'service_200' => Service::whereStatus(200)->count(),
            'service_done' => Service::whereBetween('status', [300, 400])->count(),
            'customer' => Customer::count(),
        ];

        return view('home')->with(compact('data'));
    }
}
