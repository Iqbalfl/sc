<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Service;
use App\Models\Symptom;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Session;

class SymptomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $services = Service::with('customer');
            $services->whereBetween('status', [100, 200]);
            $services = $services->select('services.*');
            
            return Datatables::of($services)
                ->addIndexColumn()
                ->addColumn('action', function($service){
                    return view('partials._action', [
                        'model'           => $service,
                        'input_url'        => route('symptom.show', $service->id),
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('symptoms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_id' => 'required|integer',
            'sn_part' => 'required|string|max:255',
            'pd_code' => 'required|string|max:255',
            'symptom' => 'required|string',
            'root_cause' => 'required|string',
            'resolution' => 'required|string',
        ]);

        $status_service = 200;
        $status_symp = 100;
        if ($request->is_no_replacing_new_part == 1){
            $status_service = 300; // finish
            $status_symp = 400; // finish
        }
        
        $symptom = new Symptom;
        $symptom->service_id = $request->service_id;
        $symptom->technician_id = auth()->user()->id;
        $symptom->sn_part = $request->sn_part;
        $symptom->pd_code = $request->pd_code;
        $symptom->symptom = $request->symptom;
        $symptom->root_cause = $request->root_cause;
        $symptom->resolution = $request->resolution;
        $symptom->status = $status_symp;
        $symptom->save();

        $service = Service::find($request->service_id);
        $service->status = $status_service;
        $service->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Symptom berhasil dibuat"
        ]);
        
        return redirect()->route('symptom.show', $request->service_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        $symptoms = Symptom::where('service_id', $id)->get();

        return view('symptoms.create')->with(compact('symptoms', 'service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id)
    {
        $status = 0;
        $symptom = Symptom::with('service', 'service.customer', 'sparePart')->where('id', $id)->first();

        if ($symptom != null){
            $status = 1;
        }

        $data = [
            'status' => $status,
            'data' => $symptom
        ];

        return response()->json($data);
    }
}
