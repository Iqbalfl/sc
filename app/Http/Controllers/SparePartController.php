<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Service;
use App\Models\Symptom;
use App\Models\SparePart;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Session;

class SparePartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderIndex(Request $request)
    {
        if ($request->ajax()) {

            $symptom = Symptom::with('service', 'service.customer')->whereStatus(100);

            $symptom = $symptom->select('symptoms.*');
            
            return Datatables::of($symptom)
                ->addIndexColumn()
                ->addColumn('action', function($service){
                    return view('partials._action_custom', [
                        'model'           => $service,
                        'input_url'        => $service->id,
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('spare_parts.order_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function orderStore(Request $request)
    {
        $this->validate($request, [
            'symptom_id' => 'required|integer',
            'wo_id' => 'required|string|max:255',
            'wo_date' => 'required|date',
        ]);
        
        $sp = new SparePart;
        $sp->symptom_id = $request->symptom_id;
        $sp->wo_id = $request->wo_id;
        $sp->wo_date = $request->wo_date;
        $sp->save();

        $symptom = Symptom::find($request->symptom_id);
        $symptom->status = 200;
        $symptom->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Sparepart berhasil disimpan"
        ]);
        
        return redirect()->route('spare_part.order.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function receivedIndex(Request $request)
    {
        if ($request->ajax()) {

            $sp = SparePart::with('symptom')->whereHas('symptom', function($q){
                $q->where('status', 200);
            });

            $sp = $sp->select('spare_parts.*');
            
            return Datatables::of($sp)
                ->addIndexColumn()
                ->addColumn('action', function($sp){
                    return view('partials._action_custom', [
                        'model'           => $sp,
                        'input_url'        => $sp->symptom->id,
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('spare_parts.received_index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function receivedStore(Request $request)
    {
        $this->validate($request, [
            'symptom_id' => 'required|integer',
            'awb_received_number' => 'required|string|max:255',
            'awb_received_date' => 'required|date',
        ]);
        
        $sp = SparePart::where('symptom_id', $request->symptom_id)->first();
        $sp->awb_received_number = $request->awb_received_number;
        $sp->awb_received_date = $request->awb_received_date;
        $sp->save();

        $symptom = Symptom::find($request->symptom_id);
        $symptom->status = 300;
        $symptom->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Sparepart berhasil disimpan"
        ]);
        
        return redirect()->route('spare_part.received.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fixingIndex(Request $request)
    {
        if ($request->ajax()) {

            $symptom = Symptom::with('service', 'sparePart')->whereStatus(300);

            $symptom = $symptom->select('symptoms.*');
            
            return Datatables::of($symptom)
                ->addIndexColumn()
                ->addColumn('action', function($service){
                    return view('partials._action_custom', [
                        'model'           => $service,
                        'input_url'        => $service->id,
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('spare_parts.fixing_index');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fixingStore(Request $request)
    {
        $this->validate($request, [
            'symptom_id' => 'required|integer',
            'status' => 'required|string|max:255',
            'finish_date' => 'required|date',
        ]);
        
        $sp = SparePart::where('symptom_id', $request->symptom_id)->first();
        $sp->status = $request->status;
        if ($request->status == 'Good' || $request->status == 'Cancel'){
            $sp->finish_date = $request->finish_date;
            $sp->good_part = $request->good_part;
        }
        $sp->save();

        if ($request->status == 'DOA' || $request->status == 'Need 2nd Part'){
            $symptom = new Symptom;
            $symptom->service_id = $sp->symptom->service_id;
            $symptom->technician_id = auth()->user()->id;
            $symptom->sn_part = $request->sn_part;
            $symptom->pd_code = $request->pd_code;
            $symptom->symptom = $request->symptom;
            $symptom->root_cause = $request->root_cause;
            $symptom->resolution = $request->resolution;
            $symptom->save();
        }

        $symptom = Symptom::find($request->symptom_id);
        $symptom->status = 400;
        $symptom->save();

        if ($request->status == 'Good' || $request->status == 'Cancel'){
            $service = Service::where('id', $symptom->service_id)->first();
            $service->status = 300;
            $service->finish_date = $request->finish_date;
            $service->save();
        }

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Data berhasil disimpan"
        ]);
        
        return redirect()->route('spare_part.fixing.index');
    }

}