<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Service;
use App\Models\SparePart;
use App\Models\Symptom;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $services = Service::with('customer');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $services->whereStatus($request->status);
                }
            }

            $services = $services->select('services.*');
            
            return Datatables::of($services)
                ->addIndexColumn()
                ->addColumn('action', function($service){
                    return view('partials._action', [
                        'model'           => $service,
                        'form_url'        => route('service.destroy', $service->id),
                        'edit_url'        => route('service.edit', $service->id),
                        'show_url'        => route('service.show', $service->id),
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::where('status', 100)->get();

        return view('services.create')->with(compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date_input' => 'required|date',
            'customer_id' => 'required|integer',
            'item_name' => 'required|string|max:255',
            'item_serial_number' => 'required|string|max:255',
            'is_waranty' => 'required|integer',
            'item_complaint' => 'required|string',
            'item_detail' => 'required|string',
        ]);
        
        $service = new Service();
        $service->code = Service::generateCode();
        $service->date_input = $request->date_input;
        $service->customer_id = $request->customer_id;
        $service->item_name = $request->item_name;
        $service->item_serial_number = $request->item_serial_number;
        $service->is_waranty = $request->is_waranty;
        $service->item_complaint = $request->item_complaint;
        $service->item_detail = $request->item_detail;
        $service->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Service dengan nomor $service->code berhasil dibuat"
        ]);

        Session::flash("do_print", true);
        
        return redirect()->route('service.show', $service->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);

        return view('services.show')->with(compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        $customers = Customer::where('status', 100)->get();

        return view('services.edit')->with(compact('service', 'customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date_input' => 'required|date',
            'customer_id' => 'required|integer',
            'item_name' => 'required|string|max:255',
            'item_serial_number' => 'required|string|max:255',
            'is_waranty' => 'required|integer',
            'item_complaint' => 'required|string',
            'item_detail' => 'required|string',
        ]);
        
        $service = Service::find($id);
        $service->date_input = $request->date_input;
        $service->customer_id = $request->customer_id;
        $service->item_name = $request->item_name;
        $service->item_serial_number = $request->item_serial_number;
        $service->is_waranty = $request->is_waranty;
        $service->item_complaint = $request->item_complaint;
        $service->item_detail = $request->item_detail;
        $service->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Service dengan nomor $service->code berhasil diubah"
        ]);
        
        return redirect()->route('service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        
        $symptoms = Symptom::where('service_id', $id)->get();

        foreach ($symptoms as $item) {
            $sp = SparePart::where('symptom_id', $item->id)->first();
            $sp->delete();

            $item->delete();
        }

        $service->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Data Service berhasil dihapus"
        ]);

        return redirect()->back();
    }

    public function print($id)
    {
        $service = Service::find($id);

        return view('services.print')->with(compact('service'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pickupIndex(Request $request)
    {
        if ($request->ajax()) {

            $services = Service::with('customer')->whereStatus(300);

            $services = $services->select('services.*');
            
            return Datatables::of($services)
                ->addIndexColumn()
                ->addColumn('action', function($service){
                    return view('partials._action_custom', [
                        'model'           => $service,
                        'input_url'       => $service->id,
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('services.pickup_index');
    }

    public function pickupStore(Request $request)
    {
        $this->validate($request, [
            'service_id' => 'required|integer',
            'pickup_date' => 'required|date',
        ]);

        $service = Service::find($request->service_id);
        $service->status = 400;
        $service->pickup_date = $request->pickup_date;
        $service->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Service Selesai"
        ]);

        return redirect()->back();
    }
}
