<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Customer;
use App\Models\Service;
use Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $customers = Customer::query();

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $customers->whereStatus($request->status);
                }
            }

            $customers = $customers->select('customers.*');
            
            return Datatables::of($customers)
                ->addIndexColumn()
                ->addColumn('action', function($customer){
                    return view('partials._action', [
                        'model'           => $customer,
                        'form_url'        => route('customer.destroy', $customer->id),
                        'edit_url'        => route('customer.edit', $customer->id),
                        'show_url'        => route('customer.show', $customer->id),
                    ]);
                })             
                ->escapeColumns([])
                ->make(true);
        }

        return view('customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            //'birth_date' => 'date',
            //'identity_number' => 'required|numeric|digits:16',
            'address' => 'required|string',
            'city' => 'required|string',
            'phone' => 'required|string|max:16',
            'email' => 'required|string|max:255',
            //'fax' => 'required|string',
            //'npwp' => 'required|string|max:16',
        ]);
        
        $customer = new Customer();
        $customer->code = Customer::generateCode();
        $customer->name = $request->name;
        $customer->birth_date = $request->birth_date;
        $customer->identity_number = $request->identity_number;
        $customer->address = $request->address;
        $customer->city = $request->city;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->fax = $request->fax;
        $customer->npwp = $request->npwp;
        $customer->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Customer dengan nama $customer->name berhasil dibuat"
        ]);
        
        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);

        return view('customers.show')->with(compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('customers.edit')->with(compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            //'birth_date' => 'date',
            //'identity_number' => 'required|numeric|digits:16',
            'address' => 'required|string',
            'city' => 'required|string',
            'phone' => 'required|string|max:16',
            'email' => 'required|string|max:255',
            //'fax' => 'required|string',
            //'npwp' => 'required|string|max:16',
            'status' => 'required|numeric',
        ]);
        
        $customer = Customer::find($id);
        $customer->name = $request->name;
        $customer->birth_date = $request->birth_date;
        $customer->identity_number = $request->identity_number;
        $customer->address = $request->address;
        $customer->city = $request->city;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->fax = $request->fax;
        $customer->npwp = $request->npwp;
        $customer->status = $request->status;
        $customer->save();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Customer dengan nama $customer->name berhasil diubah"
        ]);
        
        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);

        // cek apakah memeiliki data service
        $check = Service::where('customer_id', $customer)->count();
        if ($check > 0){
            Session::flash("flash_notification", [
                "level"=>"warning",
                "message"=>"Tidak bisa menghapus customer, karena customer memiliki data service, alternatif dapat diubah status menjadi nonaktif!"
            ]);

            return redirect()->back();
        }

        $customer->delete();

        Session::flash("flash_notification", [
            "level"=>"success",
            "message"=>"Customer berhasil dihapus"
        ]);

        return redirect()->back();
    }
}
