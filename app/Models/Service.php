<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $appends = ['display_status', 'display_finish_date', 'display_pickup_date'];

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Service Masuk';
                break;

            case '200':
                $result = 'Dalam Proses';
                break;

            case '300':
                $result = 'Menunggu Pengambilan';
                break;

            case '400':
                $result = 'Selesai';
                break;

            case '10':
                $result = 'Batal';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplayFinishDateAttribute(){
        return date_std($this->attributes['finish_date']);
    }

    public function getDisplayPickupDateAttribute(){
        return date_std($this->attributes['pickup_date']);
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }

    public static function generateCode()
    {
        $date = date('ymd');
        $data = self::whereDate('created_at', $date)->orderBy('id', 'desc')->first();
        $last_code = 'SI'.$date.'00';
        if($data != null){
            $last_code = $data->code;
        }
        $last_increment = substr($last_code, -2);
        
        return 'SI' . $date . str_pad($last_increment + 1, 2, 0, STR_PAD_LEFT);
    }
}
