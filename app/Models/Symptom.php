<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    use HasFactory;
    protected $appends = ['display_status', 'display_date'];

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Waiting Order';
                break;

            case '200':
                $result = 'Waiting Part';
                break;

            case '300':
                $result = 'Waiting Repair';
                break;

            case '400':
                $result = 'Finish';
                break;

            case '10':
                $result = 'Batal';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public function getDisplayDateAttribute(){
        return date_std($this->attributes['created_at']);
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }

    public function technician()
    {
        return $this->belongsTo('App\Models\User', 'technician_id');
    }

    public function sparePart()
    {
        return $this->hasOne('App\Models\SparePart', 'symptom_id');
    }
}
