<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $appends = ['display_role'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getDisplayRoleAttribute()
    {
        $result = '';
        $role = $this->attributes['role'];

        switch ($role) {
            case 'admin':
                $result = 'Admin';
                break;
            case 'technician':
                $result = 'Teknisi';
                break;
            case 'owner':
                $result = 'Owner';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
