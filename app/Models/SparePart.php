<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SparePart extends Model
{
    use HasFactory;
    protected $appends = ['display_received_date'];

    public function symptom()
    {
        return $this->belongsTo('App\Models\Symptom', 'symptom_id');
    }

    public function getDisplayReceivedDateAttribute(){
        return date_std($this->attributes['awb_received_date']);
    }
}
