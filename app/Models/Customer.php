<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Customer extends Model
{
    use HasFactory;

    protected $appends = ['display_status'];

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Aktif';
                break;

            case '10':
                $result = 'Tidak Aktif';
                break;
            
            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }

    public static function generateCode()
    {
        $data = self::orderBy('id', 'desc')->first();
        $last_code = 'PEL0000000';
        if($data != null){
            $last_code = $data->code;
        }
        $last_increment = substr($last_code, -7);
        
        return 'PEL' . str_pad($last_increment + 1, 7, 0, STR_PAD_LEFT);
    }
}
