<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->date('date_input');
            $table->unsignedBigInteger('customer_id');
            $table->string('item_name');
            $table->text('item_complaint');
            $table->string('item_serial_number');
            $table->text('item_detail');
            $table->boolean('is_waranty')->default(1);
            $table->unsignedInteger('cost')->nullable();
            $table->string('notes')->nullable();
            $table->string('status')->default(100);
            $table->date('finish_date')->nullable();
            $table->date('pickup_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
