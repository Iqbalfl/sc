@extends('layouts.main')

@section('content')
<div class="container-fluid">        
  <div class="row clearfix">
    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget">
        <div class="widget-body">
          <div class="d-flex justify-content-between align-items-center">
            <div class="state">
              <h6>Service Masuk</h6>
              <h2>{{ $data['service_100'] }}</h2>
            </div>
            <div class="icon">
              <i class="ik ik-cpu"></i>
            </div>
          </div>
          <small class="text-small mt-10 d-block">Total Service Masuk</small>
        </div>
        <div class="progress progress-sm">
          <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%;"></div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget">
        <div class="widget-body">
          <div class="d-flex justify-content-between align-items-center">
            <div class="state">
              <h6>Service On Prosess</h6>
              <h2>{{ $data['service_200'] }}</h2>
            </div>
            <div class="icon">
              <i class="ik ik-cpu"></i>
            </div>
          </div>
          <small class="text-small mt-10 d-block">Total Service Dalam Proses</small>
        </div>
        <div class="progress progress-sm">
          <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget">
        <div class="widget-body">
          <div class="d-flex justify-content-between align-items-center">
            <div class="state">
              <h6>Service Selesai</h6>
              <h2>{{ $data['service_done'] }}</h2>
            </div>
            <div class="icon">
              <i class="ik ik-cpu"></i>
            </div>
          </div>
          <small class="text-small mt-10 d-block">Total Service Selesai</small>
        </div>
        <div class="progress progress-sm">
          <div class="progress-bar bg-success" role="progressbar" aria-valuenow="62" aria-valuemin="0" aria-valuemax="100" style="width: 62%;"></div>
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-12">
      <div class="widget">
        <div class="widget-body">
          <div class="d-flex justify-content-between align-items-center">
            <div class="state">
              <h6>Customer</h6>
              <h2>{{ $data['customer'] }}</h2>
            </div>
            <div class="icon">
              <i class="ik ik-users"></i>
            </div>
          </div>
          <small class="text-small mt-10 d-block">Total Customer</small>
        </div>
        <div class="progress progress-sm">
          <div class="progress-bar bg-info" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%;"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <h3 class="d-block w-100">{{config('app.name')}}<small class="float-right">Date: {{date('d/m/Y')}}</small></h3></div>
    <div class="card-body">
      Hai, Selamat datang <strong>{{Auth::user()->name}}</strong>
    </div>
  </div>

</div>
@endsection

