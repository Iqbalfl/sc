@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Penerimaan Sparepart</h5>
            <span>Kelola data Penerimaan Sparepart</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Penerimaan Sparepart</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
	      <div class="card-header">
	        <h3>List Sparepart</h3>
        </div>
	      <div class="card-body">
	        <table class="table datatable">
	          <thead>
	            <tr>
	              <th>#</th>
                <th>Resolution</th>
                <th>WO ID</th>
                <th>Aksi</th>
	            </tr>
	          </thead>
	          <tbody>
              
            </tbody>
	        </table>
	      </div>
	    </div>
    </div>
  </div>

</div>

<div class="modal fade modal-add-symp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form method="POST" action="{{ route('spare_part.received.store') }}" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Received Spare Part</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="awb_received_number">AWB Received</label>
                <input type="text" class="form-control @error('awb_received_number') is-invalid @enderror" id="awb_received_number" placeholder="AWB" name="awb_received_number" value="{{old('awb_received_number')}}">
                @error('awb_received_number')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="awb_received_date">Tanggal</label>
                <input type="date" class="form-control @error('awb_received_date') is-invalid @enderror" id="awb_received_date" placeholder="Tanggal" name="awb_received_date" value="{{date('Y-m-d')}}" readonly>
                @error('awb_received_date')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="hidden" name="symptom_id" id="symptom_id" value="{{ old('symptom_id') }}">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    @if ($errors->isNotEmpty())
      $('.modal-add-symp').modal('show');
    @endif

    $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ajax: {
            url: '{{ route('spare_part.received.index') }}'
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'symptom.resolution', name: 'symptom.resolution', orderable: false, searchable: false},
            {data: 'wo_id', name: 'wo_id'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
    
  });

  function inputData(id) {

    $.get(`{{ url('/') }}/api/symptom/${id}/detail`, function( response ) {
      console.log(response)

      if (response.status == 1){
        var data = response.data;
        $('#symptom_id').val(data.id)  

        $('.modal-add-symp').modal('show');
      } else {
        alert('Error, Terjadi Kesalahan!')
      }

    });

  }
</script>
@endsection