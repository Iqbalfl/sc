@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Perbaikan Unit</h5>
            <span>Kelola data Perbaikan Unit</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Perbaikan Unit</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
	      <div class="card-header">
	        <h3>List Symptom</h3>
        </div>
	      <div class="card-body">
	        <table class="table datatable">
	          <thead>
	            <tr>
	              <th>#</th>
	              <th>Nama Barang</th>
                <th>PD Code</th>
                <th>Symptom</th>
                <th>Tanggal Received</th>
                <th>No AWB</th>
                <th>Aksi</th>
	            </tr>
	          </thead>
	          <tbody>
              
            </tbody>
	        </table>
	      </div>
	    </div>
    </div>
  </div>

</div>

<div class="modal fade modal-add-symp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form method="POST" action="{{ route('spare_part.fixing.store') }}" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Spare Part</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="status">Status Part</label>
                <select name="status" id="status_part" class="form-control select2 @error('status') is-invalid @enderror">
                  <option value="Good" {{ old('status') == 'Good' ? 'selected' : '' }}>Good</option>
                  <option value="DOA" {{ old('status') == 'DOA' ? 'selected' : '' }}>DOA</option>
                  <option value="Need 2nd Part" {{ old('status') == 'Need 2nd Part' ? 'selected' : '' }}>Need 2nd Part</option>
                  <option value="Cancel" {{ old('status') == 'Cancel' ? 'selected' : '' }}>Cancel</option>
                </select>
                @error('status')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="finish_date">Tanggal</label>
                <input type="date" class="form-control @error('finish_date') is-invalid @enderror" id="finish_date" placeholder="Tanggal" name="finish_date" value="{{date('Y-m-d')}}" readonly>
                @error('finish_date')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="good_part">Good Part</label>
                <input type="text" class="form-control @error('good_part') is-invalid @enderror" id="good_part" placeholder="Good Part" name="good_part" value="{{old('good_part')}}">
                @error('good_part')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="sn_part">SN Part</label>
                <input type="text" class="form-control @error('sn_part') is-invalid @enderror" id="sn_part" placeholder="Serial Number" name="sn_part" value="{{old('sn_part')}}" readonly>
                @error('sn_part')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="pd_code">PD Code</label>
                <input type="text" class="form-control @error('pd_code') is-invalid @enderror" id="pd_code" placeholder="PD Code" name="pd_code" value="{{old('pd_code')}}" readonly>
                @error('item_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-12 {{ $errors->has('symptom') ? ' has-error' : '' }}">
              <label for="symptom">Symptom</label>
              <textarea name="symptom" id="symptom" class="form-control @if ($errors->has('symptom')) is-invalid @enderror" rows="3" placeholder="Symptom" readonly>{{ old('symptom') }}</textarea>
              @error('symptom')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-6 {{ $errors->has('root_cause') ? ' has-error' : '' }}">
              <label for="root_cause">Root Cause</label>
              <textarea name="root_cause" id="root_cause" class="form-control @if ($errors->has('root_cause')) is-invalid @enderror" rows="3" placeholder="Root Cause" readonly>{{ old('root_cause') }}</textarea>
              @error('root_cause')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
            <div class="form-group col-6 {{ $errors->has('resolution') ? ' has-error' : '' }}">
              <label for="resolution">Resolution</label>
              <textarea name="resolution" id="resolution" class="form-control @if ($errors->has('resolution')) is-invalid @enderror" rows="3" placeholder="Resolution" readonly>{{ old('resolution') }}</textarea>
              @error('resolution')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="hidden" name="symptom_id" id="symptom_id" value="{{ old('symptom_id') }}">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    @if ($errors->isNotEmpty())
      $('.modal-add-symp').modal('show');
    @endif

    $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        autoWidth: false,
        ajax: {
          url: '{{ route('spare_part.fixing.index') }}'
        },
        columns: [
          {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
          {data: 'service.item_name', name: 'service.item_name', defaultContent: '-', orderable: false, searchable: false},
          {data: 'pd_code', name: 'pd_code'},
          {data: 'symptom', name: 'symptom'},
          {data: 'spare_part.display_received_date', name: 'created_at', orderable: false, searchable: false},
          {data: 'spare_part.awb_received_number', name: 'spare_part.awb_received_number', defaultContent: '-', orderable: false, searchable: false},
          {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
    
    $('#status_part').change();
  });

  function inputData(id) {
    $.get(`{{ url('/') }}/api/symptom/${id}/detail`, function( response ) {
      console.log(response)

      if (response.status == 1){
        var data = response.data;
        $('#symptom_id').val(data.id)
        
        $('.select2').select2({
          width: '100%'
        });
        $('.modal-add-symp').modal('show');
      } else {
        alert('Error, Terjadi Kesalahan!')
      }

    });
  }

  $('#status_part').on('change', function(){
    var val = $(this).val();

    if (val == 'Good'){
        $('#good_part').attr('readonly', false).attr('required', true);
        $('#sn_part').attr('readonly', true).attr('required', false);
        $('#pd_code').attr('readonly', true).attr('required', false);
        $('#symptom').attr('readonly', true).attr('required', false);
        $('#root_cause').attr('readonly', true).attr('required', false);
        $('#resolution').attr('readonly', true).attr('required', false);
    } else if (val == 'DOA' || val == 'Need 2nd Part'){
        $('#good_part').attr('readonly', true).attr('required', false);
        $('#sn_part').attr('readonly', false).attr('required', true);
        $('#pd_code').attr('readonly', false).attr('required', true);
        $('#symptom').attr('readonly', false).attr('required', true);
        $('#root_cause').attr('readonly', false).attr('required', true);
        $('#resolution').attr('readonly', false).attr('required', true);
    } else if (val == 'Cancel'){
        $('#good_part').attr('readonly', false).attr('required', true);
        $('#sn_part').attr('readonly', true).attr('required', false);
        $('#pd_code').attr('readonly', true).attr('required', false);
        $('#symptom').attr('readonly', true).attr('required', false);
        $('#root_cause').attr('readonly', true).attr('required', false);
        $('#resolution').attr('readonly', true).attr('required', false);
    }
  })
</script>
@endsection