@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Order Sparepart</h5>
            <span>Kelola data Order Sparepart</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Order Sparepart</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card">
	      <div class="card-header">
	        <h3>List Symptom</h3>
        </div>
	      <div class="card-body">
	        <table class="table datatable">
	          <thead>
	            <tr>
	              <th>#</th>
                <th>Customer</th>
                <th>Tanggal</th>
	              <th>Nama Barang</th>
                <th>SN Part</th>
                <th>PD Code</th>
                <th>Aksi</th>
	            </tr>
	          </thead>
	          <tbody>
              
            </tbody>
	        </table>
	      </div>
	    </div>
    </div>
  </div>

</div>

<div class="modal fade modal-add-symp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form method="POST" action="{{ route('spare_part.order.store') }}" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Order Spare Part</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="customer_name">Nama Customer</label>
                <input type="text" class="form-control @error('customer_name') is-invalid @enderror" id="customer_name" placeholder="Nama Customer" name="customer_name" value="{{old('customer_name')}}" readonly>
                @error('customer_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="customer_address">Alamat</label>
                <input type="text" class="form-control @error('customer_address') is-invalid @enderror" id="customer_address" placeholder="Alamat Customer" name="customer_address" value="{{old('customer_address')}}" readonly>
                @error('item_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="sn_part">SN Part</label>
                <input type="text" class="form-control @error('sn_part') is-invalid @enderror" id="sn_part" placeholder="Serial Number" name="sn_part" value="{{old('sn_part')}}" readonly>
                @error('sn_part')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="pd_code">PD Code</label>
                <input type="text" class="form-control @error('pd_code') is-invalid @enderror" id="pd_code" placeholder="PD Code" name="pd_code" value="{{old('pd_code')}}" readonly>
                @error('item_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-12 {{ $errors->has('symptom') ? ' has-error' : '' }}">
              <label for="symptom">Symptom</label>
              <textarea name="symptom" id="symptom" class="form-control @if ($errors->has('symptom')) is-invalid @enderror" rows="3" placeholder="Symptom" readonly>{{ old('symptom') }}</textarea>
              @error('symptom')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-6 {{ $errors->has('root_cause') ? ' has-error' : '' }}">
              <label for="root_cause">Root Cause</label>
              <textarea name="root_cause" id="root_cause" class="form-control @if ($errors->has('root_cause')) is-invalid @enderror" rows="3" placeholder="Root Cause" readonly>{{ old('root_cause') }}</textarea>
              @error('root_cause')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
            <div class="form-group col-6 {{ $errors->has('resolution') ? ' has-error' : '' }}">
              <label for="resolution">Resolution</label>
              <textarea name="resolution" id="resolution" class="form-control @if ($errors->has('resolution')) is-invalid @enderror" rows="3" placeholder="Resolution" readonly>{{ old('resolution') }}</textarea>
              @error('resolution')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="wo_id">WO ID</label>
                <input type="text" class="form-control @error('wo_id') is-invalid @enderror" id="wo_id" placeholder="WO ID" name="wo_id" value="{{old('wo_id')}}">
                @error('wo_id')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="wo_date">Tanggal</label>
                <input type="date" class="form-control @error('wo_date') is-invalid @enderror" id="wo_date" placeholder="Tanggal" name="wo_date" value="{{date('Y-m-d')}}" readonly>
                @error('wo_date')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>
          
        </div>
        <div class="modal-footer">
          <input type="hidden" name="symptom_id" id="symptom_id" value="{{ old('symptom_id') }}">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    @if ($errors->isNotEmpty())
      $('.modal-add-symp').modal('show');
    @endif

    $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ajax: {
            url: '{{ route('spare_part.order.index') }}'
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'service.customer.name', name: 'service.customer.name', defaultContent: '-'},
            {data: 'display_date', name: 'created_at'},
            {data: 'service.item_name', name: 'service.item_name', defaultContent: '-'},
            {data: 'sn_part', name: 'sn_part'},
            {data: 'pd_code', name: 'pd_code'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });
    
  });

  function inputData(id) {

    $.get(`{{ url('/') }}/api/symptom/${id}/detail`, function( response ) {
      console.log(response)

      if (response.status == 1){
        var data = response.data;

        $('#customer_name').val(data.service.customer.name)
        $('#customer_address').val(data.service.customer.address)
        $('#sn_part').val(data.sn_part)
        $('#pd_code').val(data.pd_code)

        $('#symptom_id').val(data.id)
        $('#symptom').val(data.symptom)
        $('#root_cause').val(data.root_cause)
        $('#resolution').val(data.resolution)
  
        $('.modal-add-symp').modal('show');
      } else {
        alert('Error, Terjadi Kesalahan!')
      }

    });

  }
</script>
@endsection