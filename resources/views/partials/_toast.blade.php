<script>
    @if(Session::has('flash_notification.message'))
        var type = "{{ Session::get('flash_notification.level', 'info') }}";
        switch(type){
            case 'info':
                    $.toast({
                        heading: 'Info',
                        text: '{{ Session::get('flash_notification.message') }}',
                        showHideTransition: 'slide',
                        icon: 'info',
                        loaderBg: '#46c35f',
                        position: 'bottom-right'
                    })
                break;
            
            case 'warning':
                    $.toast({
                        heading: 'Warning',
                        text: '{{ Session::get('flash_notification.message') }}',
                        showHideTransition: 'slide',
                        icon: 'warning',
                        loaderBg: '#57c7d4',
                        position: 'bottom-right'
                    })
                break;

            case 'success':
                    $.toast({
                        heading: 'Success',
                        text: '{{ Session::get('flash_notification.message') }}',
                        showHideTransition: 'slide',
                        icon: 'success',
                        loaderBg: '#f96868',
                        position: 'bottom-right'
                    })
                break;

            case 'error':
                    $.toast({
                        heading: 'Danger',
                        text: '{{ Session::get('flash_notification.message') }}',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'bottom-right'
                    })
                break;
        }
    @endif
</script>