<form class="delete" action="{{ $form_url ?? '' }}" method="post">
  {{csrf_field()}}
  {{method_field('delete')}}
  <div class="table-actions text-center">
  @isset ($show_url)
    <a href="{{ $show_url }}" title="Lihat Detail Data"><i class="ik ik-eye text-blue"></i></a>
  @endisset

  @isset ($input_url)
    <a href="{{ $input_url }}" title="Tambah Symptom"><i class="ik ik-plus text-blue"></i></a>
  @endisset
  
  @isset ($edit_url)
    <a href="{{ $edit_url }}" title="Edit Data"><i class="ik ik-edit text-yellow"></i></a>
  @endisset
  
  @isset ($form_url)
    <a href="" class="js-submit-confirm" title="Hapus Data"><i class="ik ik-trash-2 text-red"></i></a>
  @endisset
</div>
</form>

