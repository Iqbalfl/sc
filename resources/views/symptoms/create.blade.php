@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Symptom</h5>
            <span>Kelola data Symptom</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Symptom</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

	<div class="row">
	  <div class="col-md-12">
      <div class="card">
        <div class="card-header d-flex">
          <h3>Detail Service</h3>          
        </div>
        <div class="card-body">       
          @csrf
          @method('PUT')

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="date_input">Tanggal Service</label>
                <input type="date" class="form-control" placeholder="Tanggal Service" name="date_input" value="{{$service->date_input}}" readonly>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="customer_id">Nama Customer</label>
                <input type="text" class="form-control" value="{{ $service->customer->name }}" readonly>
              </div>
            </div>
            
            <div class="col-md-5">
              <div class="form-group">
                <label for="customer_address">Alamat</label>
                <input type="text" class="form-control" id="customer_address" placeholder="Alamat" name="customer_address" value="{{ $service->customer->address }}" readonly>
              </div>
            </div>
          </div>

          <hr>

          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label for="item_name">Nama Barang</label>
                <input type="text" class="form-control" id="item_name" placeholder="Nama Barang" name="item_name" value="{{$service->item_name}}" readonly>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label for="item_serial_number">SN</label>
                <input type="text" class="form-control" id="item_serial_number" placeholder="Serial Number" name="item_serial_number" value="{{$service->item_serial_number}}" readonly>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="item_serial_number">Garansi</label>
                  <div>
                    <input type="checkbox" id="switch_waranty" name="switch_waranty" class="js-single" {{ $service->is_waranty == 1 ? 'checked' : '' }} disabled />
                    <input type="hidden" name="is_waranty" id="is_waranty" value="{{ $service->is_waranty == 1 ? '1' : '0' }}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-6">
              <label for="item_complaint">Keluhan</label>
              <textarea name="item_complaint" id="item_complaint" class="form-control" rows="5" placeholder="Keluhan" readonly>{{ $service->item_complaint }}</textarea>
            </div>
            <div class="form-group col-6">
              <label for="item_detail">Kelengkapan</label>
              <textarea name="item_detail" id="item_detail" class="form-control" rows="5" placeholder="Kelengkapan" readonly>{{ $service->item_detail }}</textarea>
            </div>
          </div>
          
        </div>
      </div>

	    <div class="card">
	      <div class="card-header d-flex">
	        <h3>List Symptom</h3>
          <button type="button" class="btn btn-primary ml-auto" data-toggle="modal" data-target=".modal-add-symp"><i class="ik ik-plus"></i> Tambah</button>
        </div>
	      <div class="card-body">
	        <table class="table datatable">
	          <thead>
	            <tr>
	              <th>#</th>
	              <th>Tanggal Symptom</th>
	              <th>Teknisi</th>
                <th>PD Code</th>
                <th>Customer</th>
	            </tr>
	          </thead>
	          <tbody>
              @foreach ($symptoms as $index => $item)
                <tr>
                  <td>{{ ++$index }}</td>
                  <td>{{ date_std($item->created_at) }}</td>
                  <td>{{ $item->technician->name }}</td>
                  <td>{{ $item->pd_code }}</td>
                  <td>{{ $item->service->customer->name }}</td>
                </tr>
              @endforeach
            </tbody>
	        </table>
	      </div>
	    </div>
	  </div>
	</div>

</div>

<div class="modal fade modal-add-symp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <form method="POST" action="{{ route('symptom.store') }}" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Tambah Symptom</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="no_replacing_new_part">No Replacing New Part</label>
                <div>
                  <input type="checkbox" id="switch_no_replacing_new_part" name="switch_no_replacing_new_part" class="js-single2" />
                  <input type="hidden" name="is_no_replacing_new_part" id="is_no_replacing_new_part" value="0">
                  <input type="hidden" name="service_id"  value="{{ $service->id }}">
                </div>
              </div>
            </div>
          </div>

          <hr>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="sn_part">SN Part</label>
                <input type="text" class="form-control @error('sn_part') is-invalid @enderror" id="sn_part" placeholder="Serial Number" name="sn_part" value="{{old('sn_part')}}">
                @error('sn_part')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="pd_code">PD Code</label>
                <input type="text" class="form-control @error('pd_code') is-invalid @enderror" id="pd_code" placeholder="PD Code" name="pd_code" value="{{old('pd_code')}}">
                @error('item_name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group col-12 {{ $errors->has('symptom') ? ' has-error' : '' }}">
              <label for="symptom">Symptom</label>
              <textarea name="symptom" id="symptom" class="form-control @if ($errors->has('symptom')) is-invalid @enderror" rows="3" placeholder="Symptom">{{ old('symptom') }}</textarea>
              @error('symptom')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-6 {{ $errors->has('root_cause') ? ' has-error' : '' }}">
              <label for="root_cause">Root Cause</label>
              <textarea name="root_cause" id="root_cause" class="form-control @if ($errors->has('root_cause')) is-invalid @enderror" rows="3" placeholder="Root Cause">{{ old('root_cause') }}</textarea>
              @error('root_cause')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
            <div class="form-group col-6 {{ $errors->has('resolution') ? ' has-error' : '' }}">
              <label for="resolution">Resolution</label>
              <textarea name="resolution" id="resolution" class="form-control @if ($errors->has('resolution')) is-invalid @enderror" rows="3" placeholder="Resolution">{{ old('resolution') }}</textarea>
              @error('resolution')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection



@section('script')
<script>
  $(document).ready(function() {
    @if ($errors->isNotEmpty())
      $('.modal-add-symp').modal('show');
    @endif
    
    // Single swithces
    var elemsingle = document.querySelector('.js-single');
    var switchery = new Switchery(elemsingle, {
        color: '#4099ff',
        jackColor: '#fff'
    });

    // Single swithces
    var elemsingle = document.querySelector('.js-single2');
    var switchery = new Switchery(elemsingle, {
        color: '#4099ff',
        jackColor: '#fff'
    });
  });

  $('#switch_no_replacing_new_part').on('change', function(){
    var check = $(this).is(':checked')
    $("#is_no_replacing_new_part").val((check == true ? 1 : 0));
  })
</script>
@endsection