<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>{{ config('app.name') }}</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="_token" content="{{ csrf_token() }}">
  <meta name="base-url" content="{{ url('/') }}">

  <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />

  <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/fontawesome-free/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/icon-kit/dist/css/iconkit.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/ionicons/dist/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/jquery-toast-plugin/dist/jquery.toast.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/mohithg-switchery/dist/switchery.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/tk/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
  @yield('css')
  <link rel="stylesheet" href="{{ asset('assets/tk/dist/css/theme.min.css') }}">
  <script src="{{ asset('assets/tk/src/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>

  <div class="wrapper">
    <header class="header-top" header-theme="light">
      <div class="container-fluid">
        <div class="d-flex justify-content-between">
          <div class="top-menu d-flex align-items-center">
            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
            <div class="header-search">
              <div class="input-group">
                <span class="input-group-addon search-close"><i class="ik ik-x"></i></span>
                <input type="text" class="form-control">
                <span class="input-group-addon search-btn"><i class="ik ik-search"></i></span>
              </div>
            </div>
            <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
          </div>
          <div class="top-menu d-flex align-items-center">
            <div class="dropdown">
              <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                @if (is_null(Auth::user()->avatar))
                  <img class="avatar" src="{{ asset('assets/tk/img/avatar.jpg') }}" alt="image">
                @else
                  <img alt="image" src="{{asset('uploads/images/avatars/'.Auth::user()->avatar)}}" class="avatar">
                @endif
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('profile.show') }}"><i class="ik ik-user dropdown-icon"></i> Profile</a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                  <i class="ik ik-power dropdown-icon"></i> 
                  {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
    </header>

    <div class="page-wrap">
      <div class="app-sidebar colored">
        <div class="sidebar-header">
          <a class="header-brand" href="{{ route('home') }}">
            <div class="logo-img">
              <img src="{{ asset('assets/tk/img/logo_comp_s.png') }}" class="header-brand-img" alt="Logo" style="max-width: 35px;">
            </div>
            <span class="text">{{config('app.name')}}</span>
          </a>
          <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
          <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
        </div>

        @php
            $role = auth()->user()->role;
        @endphp

        <div class="sidebar-content">
          <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
              <div class="nav-lavel">Home</div>
              <div class="nav-item {{ setActive('home') }}">
                <a href="{{ route('home') }}"><i class="ik ik-bar-chart-2"></i><span>Dashboard</span></a>
              </div>
              <div class="nav-lavel">Menu Utama</div>
              @if ($role == 'admin') 
                <div class="nav-item {{ setActive(['customer.index', 'customer.create', 'customer.edit', 'customer.show']) }}">
                  <a href="{{ route('customer.index') }}"><i class="ik ik-layers"></i><span>Customer</span></a>
                </div>
              @endif
              @if ($role == 'admin' || $role == 'technician') 
                <div class="nav-item has-sub {{ setActive([
                    'service.index', 'service.show', 'service.create', 'service.edit', 
                    'symptom.index', 'symptom.create', 'symptom.edit', 'symptom.show',
                    'spare_part.order.index', 'spare_part.received.index', 'spare_part.fixing.index',
                    'service.pickup.index'
                  ]) }}">
                  <a href="#"><i class="ik ik-cpu"></i><span>Service</span></a>
                  <div class="submenu-content">
                    @if ($role == 'admin') 
                      <a href="{{ route('service.index') }}" class="menu-item">Data Service</a>
                    @endif

                    @if ($role == 'technician')  
                      <a href="{{ route('symptom.index') }}" class="menu-item">Symptom</a>
                    @endif
                    
                    @if ($role == 'admin') 
                      <a href="{{ route('spare_part.order.index') }}" class="menu-item">Order Sparepart</a>
                      <a href="{{ route('spare_part.received.index') }}" class="menu-item">Penerimaan Sparepart</a>
                    @endif

                    @if ($role == 'technician')  
                      <a href="{{ route('spare_part.fixing.index') }}" class="menu-item">Perbaikan Unit</a>
                    @endif

                    @if ($role == 'admin') 
                      <a href="{{ route('service.pickup.index') }}" class="menu-item">Pengambilan Unit</a>
                    @endif
                  </div>
                </div>
              @endif

              @if ($role == 'admin' || $role == 'owner') 
                <div class="nav-item {{ setActive(['report.index', 'report.create', 'report.edit', 'report.show']) }}">
                  <a href="{{ route('report.index') }}"><i class="ik ik-printer"></i><span>Laporan</span></a>
                </div>
              @endif
              <div class="nav-lavel">Akun</div>
              @if ($role == 'owner') 
                <div class="nav-item {{ setActive(['user.index', 'user.show', 'user.create', 'user.edit']) }}">
                  <div class="nav-item">
                    <a href="{{ route('user.index') }}"><i class="ik ik-users"></i><span>Pengguna</span></a>
                  </div>
                </div>
              @endif
              <div class="nav-item {{ setActive('profile.show') }}">
                <div class="nav-item">
                  <a href="{{ route('profile.show') }}"><i class="ik ik-user"></i><span>Profil</span></a>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
      <div class="main-content">
        @yield('content')
      </div>

      <footer class="footer">
        <div class="w-100 clearfix">
          <span class="text-center text-sm-left d-md-inline-block">Copyright © {{date('Y')}} {{config('app.name')}}. All Rights Reserved.</span>
          {{-- <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://lavalite.org/" class="text-dark" target="_blank">Lavalite</a></span> --}}
        </div>
      </footer>

    </div>
  </div>

  <script src="{{ asset('assets/tk/src/js/vendor/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/popper.js/dist/umd/popper.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/screenfull/dist/screenfull.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/moment/moment.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/select2/dist/js/select2.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/jquery-toast-plugin/dist/jquery.toast.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/mohithg-switchery/dist/switchery.min.js') }}"></script>
  <script src="{{ asset('assets/tk/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
  <script src="{{ asset('assets/tk/js/bs-custom-file-input.min.js') }}"></script>
  <script src="{{ asset('assets/tk/dist/js/theme.min.js') }}"></script>
  @include('partials._toast')
  @yield('script')
</body>

</html>