@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Pengguna</h5>
            <span>Kelola data pengguna</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Pengguna</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <form method="POST" action="{{ route('user.update', $user->id) }}" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">
            <h3>Edit Pengguna</h3>
          </div>
          <div class="card-body">
            @csrf
            @method('PUT')

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Nama</label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nama Lengkap" name="name" value="{{ $user->name }}">
                  @error('name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="identity_number">NIK</label>
                  <input type="text" class="form-control @error('identity_number') is-invalid @enderror" id="identity_number" placeholder="Nomor KTP" name="identity_number" value="{{ $user->identity_number }}">
                  @error('identity_number')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-7 {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @enderror" name="email" placeholder="Email" value="{{ $user->email }}">
                @error('email')
                  <div class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                  </div>
                @enderror
              </div>

              <div class="form-group col-5 {{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone">Nomor Hp</label>
                <input id="phone" type="number" class="form-control @if ($errors->has('phone')) is-invalid @enderror" name="phone" placeholder="Nomor Hp" value="{{ $user->phone }}">
                @error('phone')
                  <div class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                  </div>
                @enderror
              </div>

            </div>

            <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
              <label for="role">Hak Akses</label>
              <select name="role" class="form-control select2">
                <option value="admin" {{ $user->role=='admin' ? 'selected' : '' }}>Admin</option>
                <option value="technician" {{ $user->role=='technician' ? 'selected' : '' }}>Teknisi</option>
                <option value="owner" {{ $user->role=='owner' ? 'selected' : '' }}>Owner</option>
              </select>
              @error('role')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          
            <button type="submit" class="btn btn-primary btn-block mr-2">Submit</button>
          </div>
        </div>
      </div>

      <div class="col-lg-4">
        <div class="card card-primary">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Foto Profil</h6>
          </div>
          <div class="card-body">
            <div class="form-group">
              <div class="text-center">
                @if (is_null($user->avatar))
                  <img src="{{ asset('assets/tk/img/avatar.jpg') }}" class="rounded-circle" id="avatar-prev" width="200" height="200" alt="avatar">
                @else
                  <img alt="image" src="{{asset('uploads/images/avatars/'.$user->avatar)}}" class="rounded-circle" id="avatar-prev" width="200" height="200" alt="avatar">
                @endif
              </div>
            </div>
            <div class="form-group custom-file mb-3">
              <input id="avatar" type="file" class="custom-file-input {{ $errors->has('avatar') ? ' has-error' : '' }}" name="avatar">
              <label class="custom-file-label" for="customFile">Pilih Gambar</label>
            </div>
            @if ($errors->has('avatar'))
              <div class="invalid-feedback">
                <strong>{{ $errors->first('avatar') }}</strong>
              </div>
            @endif
          </div>
        </div>
      </div>

    </div>
  </form>

</div>
@endsection
@section('script')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#avatar-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#avatar").change(function() {
      readURL(this);
    });
  </script>
@endsection