<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | {{config('app.name')}}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../favicon.ico" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/icon-kit/dist/css/iconkit.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/ionicons/dist/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/dist/css/theme.min.css') }}">
    <script src="{{ asset('assets/tk/src/js/vendor/modernizr-2.8.3.min.js') }}"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="auth-wrapper">
      <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
          <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
            <div class="lavalite-bg" style="background-image: url('{{asset('assets/tk/img/auth/login-bg.jpg')}}')">
              <div class="lavalite-overlay"></div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
            <div class="authentication-form mx-auto">
              <div class="text-center mb-4">
                <a href="{{ url('/') }}"><img src="{{ asset('assets/tk/img/logo_comp.png') }}" alt="Logo" style="width: 180px;"></a>
              </div>
              <h3>Sign In to {{config('app.name')}}</h3>
              <p>Happy to see you again!</p>
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email" autofocus>
                  <i class="ik ik-user"></i>
                  @error('email')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <div class="form-group">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                  <i class="ik ik-lock"></i>
                  @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror
                </div>
                <div class="row">
                  <div class="col text-left">
                    <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span class="custom-control-label">&nbsp;Remember Me</span>
                    </label>
                  </div>
                  {{-- <div class="col text-right">
                    <a href="{{ route('password.request') }}">Forgot Password ?</a>
                  </div> --}}
                </div>
                <div class="sign-btn text-center">
                  <button class="btn btn-theme">Sign In</button>
                </div>
              </form>
              <div class="register">
                {{-- <p>Don't have an account? <a href="{{ route('register') }}">Create an account</a></p> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="{{ asset('assets/tk/src/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/tk/plugins/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('assets/tk/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/tk/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('assets/tk/plugins/screenfull/dist/screenfull.js') }}"></script>
    <script src="{{ asset('assets/tk/dist/js/theme.min.js') }}"></script>
  </body>
</html>