<!doctype html>
<html lang="en">

<head>
    <title>Tanda Terima</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!-- UNTUK HALAMAN SERVICEIN INPUT UNIT -->


    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/tk/plugins/icon-kit/dist/css/iconkit.min.css') }}">

    {{-- <link rel="stylesheet" href="{{ asset('admin/assets/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/assets/vendor/linearicons/style.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('admin/assets/vendor/chartist/css/chartist-custom.css') }}"> -->
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('admin/assets/css/mainnuoriplusmaterialize.css') }}"> --}}
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->


    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

    <style type="text/css">
        .pad1 {
            padding-bottom: 20px;
        }

        .logoplt {
            padding-left: 25px;
            padding-bottom: 10px;
        }

        .normalfont {
            font-weight: normal;
        }

        .boldfont {
            font-weight: bold;
        }

        .welltd {
            min-height: 20px;
            padding: 19px;
            margin-bottom: 20px;
            background-color: #f5f5f5;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            font-weight: bold;
        }

        .wellth {
            min-height: 20px;
            padding: 19px;
            margin-bottom: 20px;
            background-color: #D1D0D0;
            border: 1px solid #e3e3e3;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
            color: #FFFFFF;
        }

        .tabbor {
            border-top: 0px;
        }
        
        hr {
            border:none;
            border-top:1px dashed #000;
            color:#fff;
            background-color:#fff;
            height:1px;
        }

        @page {
            size: A4;
            margin: 0;
        }

        @media print {
            .pad1 {
                padding-bottom: 20px !important;
            }

            .no-print {
                display: none;
            }

            .wellth {
                background-color: #D1D0D0 !important;
            }

            .welltd {
                background-color: #f5f5f5 !important;
            }

            body {
                margin-top: 10px;
                background: #eee;
            }
        }

    </style>

</head>

<body>

    <!--Author      : @arboshiki-->
    <div class="container ">
        <div class="row invoice row-printable">
            <div class="col-md-9">
                <!-- col-lg-12 start here -->
                <div class="panels " id="dash_0">
                    <!-- Start .panel -->
                    <div class="panel-body ">
                        <div class="row">
                            <!-- Start .row -->
                            <table class="table boldfont">
                                <tr>
                                    <td rowspan="2"><img src="{{ asset('assets/tk/img/logo_comp.png') }}"width="250px"></td>
                                    <td align="right" valign="baseline">Service No :</td>
                                    <td>{{ $service->code }}</td>
                                </tr>
                                <tr>
                                    <td align="right">Date :</td>
                                    <td>{{ date_std($service->date_input) }}</td>
                                </tr>
                            </table>

                            <div class="col-lg-12">
                                <!-- col-lg-12 start here -->
                                <div class="invoice-items">
                                    <div class="table-responsive"
                                        style="overflow: hidden; outline: none; font-weight: normal;" tabindex="0">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th colspan="4" class="normalfont">Service Center Address :&nbsp;
                                                        Jalan Kartini No.9, Bandung<br>
                                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        &nbsp; &nbsp; Phone No :&nbsp; (022) 4264150, 4236765,
                                                        08112226058, 0812238310<br></th>
                                                </tr>
                                                <tr>
                                                    <th colspan="4" class="wellth">CUSTOMER PARTICULAR</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td width="250" class="welltd">Customer Name / Company</td>
                                                    <td colspan="3" class="text-left">{{ $service->customer->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left welltd">Address</td>
                                                    <td colspan="3" class="text-left">{{ $service->customer->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left welltd">Phone No</td>
                                                    <td colspan="3" class="text-left">{{ $service->customer->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <th colspan="4" class="wellth">MACHINE DETAIL</th>
                                                </tr>
                                                <tr>
                                                    <td class="welltd">Machine type</td>
                                                    <td width="150" class="welltd">Serial Number</td>
                                                    <td width="57" class="welltd">Warranty</td>
                                                    <td class="welltd">Problem Description / Additional Part</td>
                                                </tr>
                                                <tr>
                                                    <td>{{ $service->item_name }}</td>
                                                    <td>{{ $service->item_serial_number }}</td>
                                                    <td align="center">{{ $service->is_waranty == 1 ? 'X' : '' }}</td>
                                                    <td>{{ $service->item_complaint }}</td>
                                                </tr>
                                                <tr>
                                                    <th colspan="4" class="wellth">TERMS AND CONDITION (Syarat dan
                                                        Ketentuan)</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="4">ISI TERMS AND CONDITION (Syarat dan Ketentuan)</th>
                                                </tr>
                                                <tr>
                                                    <td class="welltd">Service Center</td>
                                                    <td colspan="2">Name</td>
                                                    <td>Signature :</td>
                                                </tr>
                                                <tr>
                                                    <th colspan="4" class="wellth">ACKNOWLEGEMENT (PRE-REPAIR)</th>
                                                </tr>
                                                <tr>
                                                    <td rowspan="2" class="welltd">Customer</td>
                                                    <td colspan="2">Date :</td>
                                                    <td rowspan="2">Signature :</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-left">Name :</td>
                                                </tr>
                                                <!--
                                                    <tr>
                                                        <th colspan="4" class="wellth">ACKNOWLEGEMENT (REPAIRED)</th>
                                                    </tr>
                                                    <tr>
                                                        <td rowspan="2" class="welltd" >Customer</td>
                                                        <td  colspan="2">Date :</td>
                                                        <td rowspan="2">Signature :</td>
                                                    </tr>
                                                    <tr class="text-left">
                                                        <td colspan="2"  class="text-left">Name :</td>
                                                    </tr>
                                                -->
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                {{-- line cutting --}}
                                <hr class="mb-4"> 

                                <table class="table boldfont">
                                    <tr>
                                        <td rowspan="2"><img src="{{ asset('assets/tk/img/logo_comp.png') }}"width="250px"></td>
                                        <td align="right" valign="baseline">Service No :</td>
                                        <td>{{ $service->code }}</td>
                                    </tr>
                                    <tr>
                                        <td align="right">Date :</td>
                                        <td>{{ date_std($service->date_input) }}</td>
                                    </tr>
                                </table>
                                <div class="col-lg-12">
                                    <div class="invoice-items">
                                        <div class="table-responsive" style="overflow: hidden; outline: none;"
                                            tabindex="0">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left welltd" width="250px">Customer Name /
                                                            Company</td>
                                                        <td>{{ $service->customer->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left welltd">Phone No</td>
                                                        <td>{{ $service->customer->phone }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left  welltd">Machine Type / SN</td>
                                                        <td>{{ $service->item_name }} / {{ $service->item_serial_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left  welltd">Problem Description</td>
                                                        <td>{{ $service->item_complaint }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left  welltd">Kelengkapan</td>
                                                        <td>{{ $service->item_detail }}</td>
                                                    </tr>
                                                    <!--
                                                        <tr>
                                                            <td width="30%" class="text-left  welltd">Engineer Name</td>
                                                            <td class="text-center" width="25%"></td>
                                                            <td class="text-center welltd" width="25%">Status</td>
                                                            <td class="text-center" width="25%"></td>
                                                        </tr>
                                                    -->
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <button type="submit" id="printInvoice" class="btn btn-primary"><i class="ik ik-printer"></i> Print</button>
                                </div>
                            </div>
                            <!-- col-lg-12 end here -->
                        </div>
                        <!-- End .row -->
                    </div>
                </div>
                <!-- End .panel -->
            </div>
            <!-- col-lg-12 end here -->
        </div>
    </div>
    <script src="{{ asset('assets/tk/src/js/vendor/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) {
                window.print();
                return true;
            }
        })
        
        $('#printInvoice').click(function() {
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) {
                window.print();
                return true;
            }
        });
    </script>


</body>

</html>
