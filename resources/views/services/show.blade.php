@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Service</h5>
            <span>Kelola Data Service</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Service</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="row">

    <div class="col-md-12">
      <div class="card">
        <div class="card-header d-flex">
          <h3>Detail Service</h3>
          <a href="{{ route('service.print', $service->id) }}" class="btn btn-primary ml-auto">
            <i class="ik ik-printer"></i> Print
          </a>
        </div>
        <div class="card-body">       
          @csrf
          @method('PUT')

          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label for="date_input">Tanggal Service</label>
                <input type="date" class="form-control" placeholder="Tanggal Service" name="date_input" value="{{$service->date_input}}" readonly>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="customer_id">Nama Customer</label>
                <input type="text" class="form-control" value="{{ $service->customer->name }}" readonly>
              </div>
            </div>
            
            <div class="col-md-5">
              <div class="form-group">
                <label for="customer_address">Alamat</label>
                <input type="text" class="form-control" id="customer_address" placeholder="Alamat" name="customer_address" value="{{ $service->customer->address }}" readonly>
              </div>
            </div>
          </div>

          <hr>

          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label for="item_name">Nama Barang</label>
                <input type="text" class="form-control" id="item_name" placeholder="Nama Barang" name="item_name" value="{{$service->item_name}}" readonly>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label for="item_serial_number">SN</label>
                <input type="text" class="form-control" id="item_serial_number" placeholder="Serial Number" name="item_serial_number" value="{{$service->item_serial_number}}" readonly>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="item_serial_number">Garansi</label>
                  <div>
                    <input type="checkbox" id="switch_waranty" name="switch_waranty" class="js-single" {{ $service->is_waranty == 1 ? 'checked' : '' }} disabled />
                    <input type="hidden" name="is_waranty" id="is_waranty" value="{{ $service->is_waranty == 1 ? '1' : '0' }}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-6">
              <label for="item_complaint">Keluhan</label>
              <textarea name="item_complaint" id="item_complaint" class="form-control" rows="5" placeholder="Keluhan" readonly>{{ $service->item_complaint }}</textarea>
            </div>
            <div class="form-group col-6">
              <label for="item_detail">Kelengkapan</label>
              <textarea name="item_detail" id="item_detail" class="form-control" rows="5" placeholder="Kelengkapan" readonly>{{ $service->item_detail }}</textarea>
            </div>
          </div>
          
        </div>
      </div>
    </div>

  </div>

</div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#customer_id').select2({
        placeholder: "Pilih atau Cari Customer",
      });

      // Single swithces
      var elemsingle = document.querySelector('.js-single');
      var switchery = new Switchery(elemsingle, {
          color: '#4099ff',
          jackColor: '#fff'
      });

      $('#customer_id').change();

      @if(Session::has('do_print'))
        print();
      @endif
    })

    $('#customer_id').on('change', function(){
      var address = $(this).find('option:selected').attr('data-address');
      $("#customer_address").val(address);
    })

    $('#switch_waranty').on('change', function(){
      var check = $(this).is(':checked')
      $("#is_waranty").val((check == true ? 1 : 0));
    })

    function print() {
      window.open("{{ route('service.print', $service->id) }}", '_blank', 'location=yes,scrollbars=yes,status=yes');
    }
  </script>
@endsection