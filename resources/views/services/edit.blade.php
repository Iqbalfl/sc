@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Service</h5>
            <span>Kelola Data Service</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Service</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <form method="POST" action="{{ route('service.update', $service->id) }}" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h3>Edit Service</h3>
          </div>
          <div class="card-body">       
            @csrf
            @method('PUT')

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label for="date_input">Tanggal Service</label>
                  <input type="date" class="form-control @error('date_input') is-invalid @enderror" id="date_input" placeholder="Tanggal Service" name="date_input" value="{{$service->date_input}}">
                  @error('date_input')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="customer_id">Nama Customer</label>
                  <select name="customer_id" id="customer_id" class="form-control @error('customer_id') is-invalid @enderror">
                    <option value=""></option>
                    @foreach ($customers as $item)
                      <option value="{{ $item->id }}" data-address="{{ $item->address }}" {{ $item->id == $service->customer_id ? 'selected' : '' }}>{{ $item->name }}</option>
                    @endforeach
                  </select>
                  @error('customer_id')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              
              <div class="col-md-5">
                <div class="form-group">
                  <label for="customer_address">Alamat</label>
                  <input type="text" class="form-control @error('customer_address') is-invalid @enderror" id="customer_address" placeholder="Alamat" name="customer_address" value="-" readonly>
                  @error('customer_address')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
            </div>

            <hr>

            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label for="item_name">Nama Barang</label>
                  <input type="text" class="form-control @error('item_name') is-invalid @enderror" id="item_name" placeholder="Nama Barang" name="item_name" value="{{$service->item_name}}">
                  @error('item_name')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <label for="item_serial_number">SN</label>
                  <input type="text" class="form-control @error('item_serial_number') is-invalid @enderror" id="item_serial_number" placeholder="Serial Number" name="item_serial_number" value="{{$service->item_serial_number}}">
                  @error('item_serial_number')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                  @enderror
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                    <label for="item_serial_number">Garansi</label>
                    <div>
                      <input type="checkbox" id="switch_waranty" name="switch_waranty" class="js-single" {{ $service->is_waranty == 1 ? 'checked' : '' }} />
                      <input type="hidden" name="is_waranty" id="is_waranty" value="{{ $service->is_waranty == 1 ? '1' : '0' }}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-6 {{ $errors->has('item_complaint') ? ' has-error' : '' }}">
                <label for="item_complaint">Keluhan</label>
                <textarea name="item_complaint" id="item_complaint" class="form-control @if ($errors->has('item_complaint')) is-invalid @enderror" rows="5" placeholder="Keluhan">{{ $service->item_complaint }}</textarea>
                @error('item_complaint')
                  <div class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                  </div>
                @enderror
              </div>
              <div class="form-group col-6 {{ $errors->has('item_detail') ? ' has-error' : '' }}">
                <label for="item_detail">Kelengkapan</label>
                <textarea name="item_detail" id="item_detail" class="form-control @if ($errors->has('item_detail')) is-invalid @enderror" rows="5" placeholder="Kelengkapan">{{ $service->item_detail }}</textarea>
                @error('item_detail')
                  <div class="invalid-feedback">
                    <strong>{{ $message }}</strong>
                  </div>
                @enderror
              </div>
            </div>

           
            <button type="submit" class="btn btn-primary btn-block mr-2">Submit</button>
            
          </div>
        </div>
      </div>

    </div>
  </form>

</div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('#customer_id').select2({
        placeholder: "Pilih atau Cari Customer",
      });

      // Single swithces
      var elemsingle = document.querySelector('.js-single');
      var switchery = new Switchery(elemsingle, {
          color: '#4099ff',
          jackColor: '#fff'
      });

      $('#customer_id').change();
    })

    $('#customer_id').on('change', function(){
      var address = $(this).find('option:selected').attr('data-address');
      $("#customer_address").val(address);
    })

    $('#switch_waranty').on('change', function(){
      var check = $(this).is(':checked')
      $("#is_waranty").val((check == true ? 1 : 0));
    })
  </script>
@endsection