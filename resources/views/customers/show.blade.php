@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Customer</h5>
            <span>Kelola Data Customer</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Customer</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  
  <div class="row">

    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3>Detail Customer</h3>
        </div>
        <div class="card-body">       

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Nama Lengkap" name="name" value="{{ $customer->name }}" readonly>
              </div>
            </div>
            {{-- <div class="col-md-3">
              <div class="form-group">
                <label for="birth_date">Tanggal Lahir</label>
                <input type="date" class="form-control @error('birth_date') is-invalid @enderror" id="birth_date" placeholder="Nomor KTP" name="birth_date" value="{{$customer->birth_date}}" readonly>
              </div>
            </div> --}}
          </div>

          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label for="address">Alamat</label>
                <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" placeholder="Alamat" name="address" value="{{$customer->address}}" readonly>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="city">Kota</label>
                <input type="text" class="form-control @error('city') is-invalid @enderror" id="city" placeholder="Kota" name="city" value="{{$customer->city}}" readonly>
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="form-group col-5 {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone">Nomor HP</label>
              <input id="phone" type="number" class="form-control @if ($errors->has('phone')) is-invalid @enderror" name="phone" placeholder="Nomor HP" value="{{ $customer->phone }}" readonly>
            </div>

            <div class="form-group col-7 {{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control @if ($errors->has('email')) is-invalid @enderror" name="email" placeholder="Email" value="{{ $customer->email}}" readonly>
            </div>
          </div>

          <div class="row">
            
            <div class="col-md-4">
              <div class="form-group">
                <label for="identity_number">NIK</label>
                <input type="text" class="form-control @error('identity_number') is-invalid @enderror" id="identity_number" placeholder="Nomor KTP" name="identity_number" value="{{$customer->identity_number}}" readonly>
              </div>
            </div>

            <div class="form-group col-4 {{ $errors->has('fax') ? ' has-error' : '' }}">
              <label for="fax">Fax</label>
              <input id="fax" type="number" class="form-control @if ($errors->has('fax')) is-invalid @enderror" name="fax" placeholder="Fax" value="{{ $customer->fax}}" readonly>
            </div>

            <div class="form-group col-4 {{ $errors->has('npwp') ? ' has-error' : '' }}">
              <label for="npwp">NPWP</label>
              <input id="npwp" type="number" class="form-control @if ($errors->has('npwp')) is-invalid @enderror" name="npwp" placeholder="Email" value="{{ $customer->npwp }}" readonly>
              @error('email')
                <div class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                </div>
              @enderror
            </div>
          </div>

          <div class="row">
            <div class="form-group col-4 {{ $errors->has('status') ? ' has-error' : '' }}">
              <label for="status">Status</label>
              <select name="status" class="form-control select2" disabled>
                <option value="100" {{ $customer->status=='100' ? 'selected' : '' }}>Aktif</option>
                <option value="10" {{ $customer->status=='10' ? 'selected' : '' }}>Tidak Aktif</option>
              </select>
            </div>
          </div>

          
        </div>
      </div>
    </div>

  </div>
  

</div>
@endsection

@section('script')
  <script>
    $(document).ready(function () {
      $('.select2').select2();
    })
  </script>
@endsection