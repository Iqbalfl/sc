@extends('layouts.main')

@section('content')
<div class="container-fluid"> 

	<div class="page-header">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="page-header-title">
          <i class="ik ik-bookmark bg-blue"></i>
          <div class="d-inline">
            <h5>Customer</h5>
            <span>Kelola data Customer</span>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <nav class="breadcrumb-container" aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('home') }}"><i class="ik ik-home"></i></a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Customer</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>

  <div class="d-sm-flex align-items-center justify-content-start mb-4">
    <a class="btn btn-sm btn-primary mr-2" href="{{ route('customer.create') }}"><i class="ik ik-plus"></i> Tambah Data</a>
    <div class="form-inline ml-auto">
      <label for="status">Filter Status</label>
      <select name="status" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        <option value="100">Aktif</option>
        <option value="10">Tidak Aktif</option>
      </select>
    </div>
  </div>

	<div class="row">
	  <div class="col-md-12">
	    <div class="card">
	      <div class="card-header">
	        <h3>List Customer</h3></div>
	      <div class="card-body">
	        <table class="table datatable">
	          <thead>
	            <tr>
	              <th>#</th>
	              <th>Kode</th>
	              <th>Nama</th>
                <th>Alamat</th>
                <th>No Hp</th>
                <th>Status</th>
	              <th class="text-right">Aksi</th>
	            </tr>
	          </thead>
	          <tbody></tbody>
	        </table>
	      </div>
	    </div>
	  </div>
	</div>

</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          ajax: {
            url: '{{ route('customer.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'code', name: 'code'},
            {data: 'name', name: 'name'},
            {data: 'address', name: 'address', defaultContent: '-'},
            {data: 'phone', name: 'phone'},
            {data: 'display_status', name: 'display_status', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('select[name=status]').change(function(){
         $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });
  });
</script>
@endsection